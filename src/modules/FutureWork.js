let FutureWork = {

  commands: {
    disableInactivityWarnings: "08003c000400150000000000",
    enableDisconnectNotification: "060c000100000000",
    requestAlarms: "0d",
    requestGPS: "0e",
    inactivitySchedule: "0801",
    doNotDisturbOff: "0982",
    doNotDisturbAutomatic: "0983",
    unlock: "06160000",
    lock: "06160001",
    reboot: "05",
    factoryReset: "060b0001",
    enableDisplayCaller: "0610000001",
    disableDisplayCaller: "0610000000",

  },

  // TODO: check if this is necessary, else remove
  requestGPSVersion: async function() {
    // TODO: register to config notifications and resolving promise depending
    let packet = this.hexStringToHexBuffer(this.messages.setup.requestGPS);
    return this.sendWithoutResponse(
      this.mibandCustomService0,
      this.configCharacteristic,
      packet
    );
  },
  /**
   * Removes the lock of the screen
   */
  unlockScreen: async function() {
    // TODO: register to config notifications and resolve promise according to message
    let packet = this.hexStringToHexBuffer(this.messages.setup.unlock);
    return this.sendWithoutResponse(
      this.mibandCustomService0,
      this.configCharacteristic,
      packet
    );
  },

  /**
   * Sets DND on or off
   * Currently only sets it off
   * @param {boolean} on
   */
  setDoNotDisturb: async function(on) {
    // TODO: register to config charac
    let packet = this.hexStringToHexBuffer(this.messages.setup.doNotDisturbOff);
    return this.sendWithoutResponse(
      this.mibandCustomService0,
      this.configCharacteristic,
      packet
    );
  },

  /**
   * Switches scroll no wrist move
   * Currently only switches it off
   * @param {boolean} on
   */
  setScrollOnMoveWrist: async function(on) {
    // TODO: register to characteristic
    let packet = this.hexStringToHexBuffer(this.messages.setup.scrollWristOff);
    return this.sendWithoutResponse(
      this.mibandCustomService0,
      this.configCharacteristic,
      packet
    );
  },

    /**
   * Displays caller
   * Currently only disables it
   * @param {boolean} on
   */
  setupDisplayCaller: async function(on) {
    // TODO: register to characteristic
    let packet = this.hexStringToHexBuffer(
      this.messages.setup.enableDisplayCaller
    );
    return this.sendWithoutResponse(
      this.mibandCustomService0,
      this.configCharacteristic,
      packet
    );
  },

  /**
   * dnd stands for do not disturb. Aka when the inactivity warnings will be shut off.
   * All dates are sent in two byte formats (hours, minutes)
   * After a few tests, even with Gadgetbridge, it seems the inactivity warnings are not particularly accurate in terms of going off every minute.
   * Probably using a 60 minute interval it should work. Needs to be tested. 
   * @param {Date} startDate start -> end, when inactivity warnings will be on
   * @param {Date} endDate
   * @param {Date} dndStartDate dndStart -> dndEnd, when inactivity warnings will be off
   * @param {Date} dndEndDate
   * @param {Int} threshold how much time before inactivity warning shows up (minutes)
   */
  
  setInactivityWarnings: async function(
    startDate,
    endDate,
    dndStartDate,
    dndEndDate,
    threshold
  ) {
    // TODO: register to characteristic
    let packet = "";
    let message = this.messages.setup.inactivitySchedule;
    message += this.createByteStringFromInt(threshold) + "00";

    let startHour = this.createByteStringFromInt(startDate.getHours());
    let startMinutes = this.createByteStringFromInt(startDate.getMinutes());
    let endHour = this.createByteStringFromInt(endDate.getHours());
    let endMinutes = this.createByteStringFromInt(endDate.getMinutes());

    message += startHour + startMinutes;

    if (dndStartDate !== null && dndEndDate != null) {
      let dndStartHour = this.createByteStringFromInt(dndStartDate.getHours());
      let dndStartMinutes = this.createByteStringFromInt(
        dndStartDate.getMinutes()
      );
      let dndEndHour = this.createByteStringFromInt(dndEndDate.getHours());
      let dndEndMinutes = this.createByteStringFromInt(dndEndDate.getMinutes());
      message +=
        dndStartHour +
        dndStartMinutes +
        dndEndHour +
        dndEndMinutes +
        endHour +
        endMinutes;
    } else {
      message += endHour + endMinutes + "00" + "00" + "00" + "00";
    }

    packet = this.hexStringToHexBuffer(message);

    await this.sendAndVerifyConfiguration(
      this.mibandCustomService0,
      this.configCharacteristic,
      packet
    );
  },

  startEventNotifications: function() {
    this.registerNotification(
      this.mibandCustomService0,
      this.eventCharacteristic
    );
    ble.startNotification(
      this.deviceId,
      this.mibandCustomService0,
      this.eventCharacteristic,
      dataResponse => {
        let value = Buffer.from(dataResponse);
        console.log("Event value: " + value.toString("hex"));
      },
      failure => {
        console.log(failure);
      }
    );
  },
};
