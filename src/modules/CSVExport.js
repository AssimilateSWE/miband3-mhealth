var CSVExport = {
  storedCSV: undefined,
  storedHRData: [],
  storedAccData: [],

  requestFileSystem: async function() {
    return new Promise((res, rej) => {
        window.requestFileSystem(
            LocalFileSystem.PERSISTENT,
            0,
            fs => {
              console.log("File system open:", fs.name);
              fs.root.getFile(
                "HRFile.csv",
                { create: true, exclusive: false },
                fileEntry => {
                    console.log(fileEntry.fullPath, fileEntry);
                  res(fileEntry);
                },
                onErrorCreateFile => {
                  console.log(onErrorCreateFile);
                  rej()
                }
              );
            },
            onErrorLoadFs => {
              console.log(onErrorLoadFs);
              rej()
            }
          );
    })
  },

  writeFile: function(fileEntry, dataObj) {
    fileEntry.createWriter(fileWriter => {
      fileWriter.onwriteend = () => {
        console.log("Successful file write");
        this.readFile(fileEntry);
      };
      fileWriter.onerror = e => {
        console.log("Failed file write: " + e.toString());
      };

      if (!dataObj)
        [
          (dataObj = new Blob(["Hello World file data!"], { type: "text/csv" }))
        ];
      fileWriter.write(dataObj);
    });
  },

  readFile: function(fileEntry) {
    fileEntry.file(
      function(file) {
        var reader = new FileReader();

        reader.onloadend = function() {
          console.log("Successful file read:", this.result);
          //displayFileData(fileEntry.fullPath + ': ' + this.result)
        };
        reader.readAsText(file);
      },
      onErrorReadFile => {
        console.log("Error reading file:", onErrorReadFile);
      }
    );
  },

  convertToCSV: function () {
    let csvContent = "data:text/csv;charset=utf-8,"
    this.storedHRData.forEach((rowArray) => {
        let row = rowArray.join(",")
        csvContent += row + "\r\n"
    })
    this.storedCSV = csvContent
  },

  /**
   * Convert to an array which contains arrays with only two values, data and time in each array.
   *  */ 

  addTimeToData: function () {
    let dataArrayWithTime = []
    let storedHRDataWithTime = []
    let timeCounter = 0
    for(let data of this.storedHRData) {
        let timeLength = 1/(data.length)
        for(let dataPoint of data) {
            dataArrayWithTime.push(dataPoint)
            dataArrayWithTime.push(timeCounter.toFixed(2))
            storedHRDataWithTime.push(dataArrayWithTime)
            dataArrayWithTime = []
            timeCounter += timeLength
        }
    }
    this.storedHRData = storedHRDataWithTime
  },

  addNewHRData: function (data) {
    this.storedHRData.push(data)
  },

  getStoredHRData: function () {
    return this.storedHRData
  },

  deleteHRData: function () {
    this.storedHRData = []
  },

  addNewAccData: function (data) {
      this.storedAccData.push(data)
  },

  getStoredAccData: function () {
    return this.storedAccData
  },

  deleteAccData: function () {
    this.storedAccData = []
  },

  getStoredCSV: function() {
    return this.storedCSV;
  },

};

export default CSVExport;
