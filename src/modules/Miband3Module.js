/**
 * Author: Daniel Abella with the help of
 * Dario Salvi
 * based on work done by Volodymyr Shymanskyy - https://github.com/vshymanskyy/miband-js
 * and work done by José Rebelo as well as work done by the good folks at https://gadgetbridge.org/
 */

// Imports

import crypto_aes from "browserify-aes";
import CustomDate from "../modules/CustomDate";

const customUUID = x => `0000${x}-0000-3512-2118-0009af100700`;
const standardUUID = x => `0000${x}-0000-1000-8000-00805f9b34fb`;

var Miband3 = {
  authenticationKey: undefined,
  deviceId: undefined,
  // services
  hrMonitorService: standardUUID("180d"),
  mibandCustomService0: standardUUID("fee0"),
  mibandCustomService1: standardUUID("fee1"),
  // characteristics
  authenticationCharacteristic: customUUID("0009"),
  notificationCharacteristic: customUUID("0003"),
  hrMonitorControlCharacteristic: standardUUID("2a39"),
  hrMonitorMeasureCharacteristic: standardUUID("2a37"),
  timeCharacteristic: standardUUID("2a2b"),
  batteryCharacteristic: customUUID("0006"),
  deviceInformationService: standardUUID("180a"),
  hardwareCharacteristic: standardUUID("2a27"),
  softwareCharacteristic: standardUUID("2a28"),
  storageControlCharacteristic: customUUID("0004"),
  storageDataCharacteristic: customUUID("0005"),
  rawDataCharacteristic: customUUID("0002"),
  configCharacteristic: customUUID("0003"),
  manufacturerNameCharacteristic: standardUUID("2a29"),
  modelNumberCharacteristic: standardUUID("2a24"),
  serialNumberCharacteristic: standardUUID("2a25"),
  firmwareCharacteristic: standardUUID("2a26"),
  userCharacteristic: customUUID("0008"),
  stepCountCharacteristic: customUUID("0007"),
  sensorCharacteristic: customUUID("0001"),

  crypto: window.crypto || window.msCrypto,
  textDecoder: new TextDecoder(),

  messages: {
    authentication: {
      keySentOK: "100101",
      encryptionValueReceived: "100201",
      notAuthenticated: "100304",
      authenticated: "100301",
      sendKey: "01",
      requestEncryptionValue: "02",
      sendEncryptedKey: "03",
      authFlag: "00"
    },
    heartRate: {
      turnOffHROneShot: "150200",
      turnOffHRContinuous: "150100",
      turnOnHRContinuous: "150101",
      ping: "16"
    },
    rawData: {
      rawHRAndACC: "010319",
      rawHR: "010219",
      rawAcc: "010119",
      ping: "02"
    },
    storedData: {
      activityType: "01",
      response: "01",
      startDate: "01",
      success: "01",
      failure: "00" // Not sure if corret...
    },
    setup: {
      dateTimeFormat: "060a00",
      hourFormat: "060200",
      distance: "060300",
      changeScreens: [
        "0a",
        "01",
        "30",
        "00",
        "00",
        "00",
        "06",
        "00",
        "00",
        "00",
        "00",
        "00"
      ],
      sendUserInfo: "4f",
      setLanguage: "061700",
      setUserInfo: "0x4f",
      dateFormat: "061e00", // After 00 --> MM/dd/yyyy in hex characters
      nightMode: "1a",
      wearLocation: "200000",
      fitnessGoalStart: "100000",
      fitnessGoalEnd: "0000",
      defaultStepGoal: "8000",
      scrollWristOff: "060d0000",
      displayOnLift: "060500",
      goalNotification: "060600",
      enableHRSleepSupport: "150001",
      disableHRSleepSupport: "150000",
      HRExpose: "060100",
      setHRInterval: "14",
      response: "10",
      okResponse: "01"
    }
  },

  // this is used to keep track of running notifications
  runningNotifications: new Map(),

  // TODO: remove, just use the keys in Map
  runningNotificationCharacteristics: [],

  /**
   * Initialises the instance of the MiBand3 driver
   * @param {string} deviceId this is the device id provide dby the ble-central plugin (MAC address)
   * @param {string} key authentication key used for this device
   */
  init: async function(deviceId, key) {
    console.log("Enabling with device:", deviceId);
    this.deviceId = deviceId;
    this.authenticationKey = key;
    return;
  },

  /**
   * Connects to the device using ble-central, does not authenticate
   */
  connect: async function() {
    return new Promise((res, rej) => {
      ble.connect(
        this.deviceId,
        success => {
          console.log("Connection successful");
          res(success);
        },
        error => {
          console.log("Connection error:", error);
          rej(error);
        }
      );
    });
  },

  /**
   * Disconnects the Miband3 device
   * TODO: Implement error callback, check behaviour of disconnect on errorCallbacks
   */
  disconnect: async function() {
    if (this.deviceId === null || this.deviceId === undefined) {
      return Promise.resolve();
    } else {
      return new Promise((res, rej) => {
        ble.disconnect(
          this.deviceId,
          success => {
            console.log("Disconnected:", this.deviceId);
            res(success);
          },
          failure => {
            console.log("Failed to disconnect:", failure);
            rej(failure);
          }
        );
      });
    }
  },

  /**
   * Tells if the drivers is connected to a device
   */
  isConnected: async function() {
    return new Promise((res, rej) => {
      ble.isConnected(
        this.deviceId,
        success => {
          console.log("Is connected", success);
          res(true);
        },
        error => {
          console.log("Error is connected", error);
          res(false);
        }
      );
    });
  },

  fullAuthentication: async function() {
    return this.sendAuthenticationKey();
  },

  halfAuthentication: async function() {
    return this.requestEncryptionValue();
  },

  /**
   * Authenticate the device
   * @param {boolean} deviceAuthenticated if true, the device has already been authenticated once
   */
  authenticate: async function(deviceAuthenticated) {
    this.registerNotification(
      this.mibandCustomService1,
      this.authenticationCharacteristic
    );

    return new Promise((res, rej) => {
      console.log("Current key auth:", this.authenticationKey);
      ble.startNotification(
        this.deviceId,
        this.mibandCustomService1,
        this.authenticationCharacteristic,
        dataResponse => {
          console.log("Device Id authentication:", this.deviceId);
          let value = Buffer.from(dataResponse);
          const command = value.slice(0, 3).toString("hex");

          if (command === this.messages.authentication.keySentOK) {
            let currentDate = new Date();
            this.setTimeStatus(currentDate);
            this.requestEncryptionValue();
          } else if (
            command === this.messages.authentication.encryptionValueReceived
          ) {
            let encryptionValue = value.slice(3);
            let encryptedKey = this.createEncryptedKey(encryptionValue);
            this.sendEncryptedKey(encryptedKey);
          } else if (
            command === this.messages.authentication.notAuthenticated
          ) {
            console.log("Encryption key auth failed");
            rej();
          } else if (command === this.messages.authentication.authenticated) {
            console.log("Authenticated...");
            res();
            // TODO: Can't currently stop notifications and start another one, issue raised: https://github.com/don/cordova-plugin-ble-central/issues/552
          }
        },
        failure => {
          console.log(failure);
          rej();
        }
      );

      if (!deviceAuthenticated) {
        console.log("Full authentication");
        this.fullAuthentication(this.device);
      } else {
        console.log("Half authentication");
        this.halfAuthentication(this.device);
      }
    });
  },

  sendAuthenticationKey: function() {
    let packet = this.hexStringToHexBuffer(
      this.messages.authentication.sendKey +
        this.messages.authentication.authFlag +
        this.authenticationKey
    );
    this.sendWithoutResponse(
      this.mibandCustomService1,
      this.authenticationCharacteristic,
      packet
    );
  },

  requestEncryptionValue: function() {
    let packet = this.hexStringToHexBuffer(
      this.messages.authentication.requestEncryptionValue +
        this.messages.authentication.authFlag
    );
    this.sendWithoutResponse(
      this.mibandCustomService1,
      this.authenticationCharacteristic,
      packet
    );
  },

  createEncryptedKey: function(encryptionValue) {
    let keyAsBuffer = Buffer.from(this.authenticationKey, "hex");
    let cipher = crypto_aes
      .createCipheriv("aes-128-ecb", keyAsBuffer, "")
      .setAutoPadding(false);
    let encrypted = Buffer.concat([
      cipher.update(encryptionValue),
      cipher.final()
    ]);
    return encrypted;
  },

  sendEncryptedKey: function(encryptedKey) {
    let packet = this.hexStringToHexBuffer(
      this.messages.authentication.sendEncryptedKey +
        this.messages.authentication.authFlag +
        encryptedKey.toString("hex")
    );
    this.sendWithoutResponse(
      this.mibandCustomService1,
      this.authenticationCharacteristic,
      packet
    );
  },

  // TODO: this would need to become part of an external module
  // TODO: add synch watch time with phone functionality here,
  setupDevice: async function() {
    console.log('Setting up device...');
    await this.setLanguage('EN_en'); // Works 100%
    await this.setNightMode(null, null); // Should work... UX friendly if activated
    await this.setDateFormat(true); // Should work?...
    await this.setDisplayDateTime();
    await this.setTimeFormat('24h'); // Works 100%
    //await this.setUser(); // Maybe doesn't work...
    await this.setDistanceType(true); //
    await this.setWearLocation(true);
    await this.getBatteryStatus();
    let screens = ["activity", "heartRate", "status"];
    await this.setupScreens(screens); // Hmm?? Should show one page (clock page) but doesn't...
    await this.activateDisplayOnWristLift('on'); // UX friendly if activated
    await this.setHRSleepSupport(true)
    await this.setExposeHRToThirdParty(true)
    return this.setHeartRateMeasurementInterval(1) // Does not currently resolve, need to make this happen
  },

  // TODO: use this when you actually need it, and resolve the promise according to the message you get
  /**
   * It seems that messages can be sent to the config characteristic if the preamble command is correct, no matter the length of the packet.
   * @param {*} service
   * @param {*} characteristic
   * @param {*} packet
   * @param {*} okResponse
   */
  sendAndVerifyConfiguration: async function(
    service,
    characteristic,
    packet,
    command
  ) {
    this.registerNotification(service, characteristic);
    return new Promise((res, rej) => {
      ble.startNotification(
        this.deviceId,
        service,
        characteristic,
        dataResponse => {
          let response = Buffer.from(dataResponse).toString("hex");
          console.log("Config reponse:", response);
          console.log(command);
          let responseMessage = this.messages.setup.response;
          let okResponse = this.messages.setup.okResponse;
          if (response === responseMessage + command + okResponse) {
            console.log("OK reponse");
            res();
          }
          //TODO: check if i can unsubscribe to all notifications once authenticated and configurations are sent.
        },
        failure => {
          console.log("Config fail:", failure);
          rej(failure);
        }
      );
      this.sendWithoutResponse(service, characteristic, packet);
    });
  },

  /**
   * Sets the device's language
   * @param {string} lang the language to be set, only english supported currently. The format is: en_US, or fr_FR for example.
   */
  setLanguage: async function(lang) {
    // lang is currently ignored, it always sets english
    // TODO: register to config notifications and make sure you receive an OK
    let command = this.messages.setup.setLanguage;
    let packet = this.hexStringToHexBuffer(
      command + this.convertLangAndCountryStringToHex(lang)
    );
    await this.sendAndVerifyConfiguration(
      this.mibandCustomService0,
      this.configCharacteristic,
      packet,
      command
    );
  },

  convertLangAndCountryStringToHex(langAndCountry) {
    if (langAndCountry.length !== 5) return;
    return Buffer.from(langAndCountry).toString("hex");
  },

  /**
   * Sets night mode
   * @param {Date} startTime start time, if null or undefined, night mode is always OFF
   * @param {Date} endTime end time
   */
  setNightMode: async function(startTime, endTime) {
    let packet = "";
    let command = "";
    if (startTime !== null && endTime !== null) {
      let startHour = this.createByteStringFromInt(startTime.getHours());
      let startMinute = this.createByteStringFromInt(startTime.getMinutes());
      let endHour = this.createByteStringFromInt(endTime.getHours());
      let endMinute = this.createByteStringFromInt(endTime.getMinutes());
      let schedule = "01";
      command = this.messages.setup.nightMode;
      let message = startHour + startMinute + endHour + endMinute;
      packet = this.hexStringToHexBuffer(command + schedule + message);
    } else {
      console.log('Night mode off...')
      let off = "00";
      command = this.messages.setup.nightMode;
      packet = this.hexStringToHexBuffer(command + off);
    }
    // TODO: actually set night mode at given times
    // TODO: register to config notifications and resolve promise according to message
    await this.sendAndVerifyConfiguration(
      this.mibandCustomService0,
      this.configCharacteristic,
      packet,
      command
    );
  },

  /**
   * Decide if you want date and time, or only one of those
   * Currently only allows date and time, doesn't seem to set only time, and such a setting doesn't exist in Gadgetbridge app.
   */
  setDisplayDateTime: async function() {
    // TODO: register to config notifications and resolve promise according to message
    let dateAndTime = "03";
    let timeOnly = "00"; // Does not work currently, for some reason.
    let command = this.messages.setup.dateTimeFormat;
    let packet = this.hexStringToHexBuffer(
      command + dateAndTime // TODO: check if this is time only, or time AND date
    );
    await this.sendAndVerifyConfiguration(
      this.mibandCustomService0,
      this.configCharacteristic,
      packet,
      command
    );
  },

  /**
   * Sets time format: 24h or 12h
   * Defaults to 24h
   * @param {string} format "24h" or "12h"
   */
  setTimeFormat: async function(format) {
    // TODO: register to config notifications and resolve promise according to message
    let packet = "";
    let command = this.messages.setup.hourFormat;
    if (format === "12h") {
      let twelveHourFormat = "00";
      packet = this.hexStringToHexBuffer(command + twelveHourFormat);
    } else {
      let twentyFourHourFormat = "01";
      packet = this.hexStringToHexBuffer(command + twentyFourHourFormat);
    }
    await this.sendAndVerifyConfiguration(
      this.mibandCustomService0,
      this.configCharacteristic,
      packet,
      command
    );
  },

  /**
   * Sets the format of the date
   * Currently only sets day/month/year
   * @param {boolean} dayFirst if true, the format is day/month/year, else it's month/day/year
   */
  setDateFormat: async function(dayFirst) {
    // TODO: register to config notifications and resolve promise according to message
    let packet = "";
    let format = "";
    let message = "";
    if (dayFirst) {
      console.log("Setting day first");
      format = "dd/MM/yyyy";
      message = Buffer.from(format).toString("hex");
    } else {
      format = "MM/dd/yyyy";
      message = Buffer.from(format).toString("hex");
    }
    let command = this.messages.setup.dateFormat;
    packet = this.hexStringToHexBuffer(command + message);
    await this.sendAndVerifyConfiguration(
      this.mibandCustomService0,
      this.configCharacteristic,
      packet,
      command
    );
  },

  /**
   * Sets the distance metric: meters or feet
   * Currently only supports meters
   * @param {boolean} meters if true uses meters, else uses feet
   */
  setDistanceType: async function(meters) {
    // TODO: register to config charac and check the answer
    let packet = "";
    let message = "";
    let command = this.messages.setup.distance;
    if (meters) {
      let metric = "00";
      message = command + metric;
    } else {
      let imperial = "01";
      message = command + imperial;
    }
    packet = this.hexStringToHexBuffer(message);
    await this.sendAndVerifyConfiguration(
      this.mibandCustomService0,
      this.configCharacteristic,
      packet,
      command
    );
  },

  /**
   * Sets the wear position
   * Currently only supports right
   * @param {boolean} right if true watch is worn on the right
   */
  // TODO: implement also left
  setWearLocation: async function(right) {
    // TODO: register to user charac and check the answer
    let packet = "";
    let message = "";
    let command = this.messages.setup.wearLocation;
    if (right) {
      let right = "82";
      message = command + right;
    } else {
      let left = "02";
      message = command + left;
    }
    packet = this.hexStringToHexBuffer(message);
    await this.sendWithResponse(
      this.mibandCustomService0,
      this.userCharacteristic,
      packet
    );
  },

  /**
   * Sets the goal in steps
   * @param {number} goalSteps
   */
  setStepGoal: async function(goalSteps) {
    let stepsHex = this.createByteStringFromInt(goalSteps);
    if (stepsHex.length === 2) {
      stepsHex = stepsHex + "00";
    } else if (stepsHex.length === 4) {
      stepsHex = stepsHex.substring(2, 4) + stepsHex.substring(0, 2);
    }

    let packet = this.hexStringToHexBuffer(
      this.messages.setup.fitnessGoalStart +
        stepsHex +
        this.messages.setup.fitnessGoalEnd
    );

    await this.sendWithResponse(
      this.mibandCustomService0,
      this.userCharacteristic,
      packet
    );
  },

  /**
   * Sets screens and its order
   * A small battery icon is shown when the battery is running out on the main clock screen.
   * However if the user is to see the battery status, the status screen needs to be enabled which also includes the
   * step count.
   * @param {string[]} screens an array of "clock", "heartRate", "status", "more", "notifications", "activity" and "weather"
   */
  setupScreens: async function(screens) {
    let packet = "";
    let messageArray = this.messages.setup.changeScreens;
    let command = "0a";
    let pos = 1;
    if (screens.includes("notifications")) {
      messageArray[1] = this.performOROperationOnStringBytes(
        messageArray[1],
        "02"
      );
      messageArray[4] = this.parsePositionToString(pos);
      pos++;
    }
    if (screens.includes("weather")) {
      messageArray[1] = this.performOROperationOnStringBytes(
        messageArray[1],
        "04"
      );
      messageArray[5] = this.parsePositionToString(pos);
      pos++;
    }
    if (screens.includes("activity")) {
      messageArray[1] = this.performOROperationOnStringBytes(
        messageArray[1],
        "08"
      );
      messageArray[6] = this.parsePositionToString(pos);
      pos++;
    }
    if (screens.includes("more")) {
      messageArray[1] = this.performOROperationOnStringBytes(
        messageArray[1],
        "10"
      );
      messageArray[7] = this.parsePositionToString(pos);
      pos++;
    }
    if (screens.includes("status")) {
      messageArray[1] = this.performOROperationOnStringBytes(
        messageArray[1],
        "20"
      );
      messageArray[8] = this.parsePositionToString(pos);
      pos++;
    }
    if (screens.includes("heartRate")) {
      messageArray[1] = this.performOROperationOnStringBytes(
        messageArray[1],
        "40"
      );
      messageArray[9] = this.parsePositionToString(pos);
      pos++;
    }
    if (screens.includes("timer")) {
      messageArray[1] = this.performOROperationOnStringBytes(
        messageArray[1],
        "80"
      );
      messageArray[10] = this.parsePositionToString(pos);
      pos++;
    }
    for (let i = 4; i <= 11; i++) {
      if (messageArray[i] === "00") {
        messageArray[i] = this.paddHex(parseInt(pos++, 16).toString(16));
      }
    }
    let reducer = (accumulator, currentValue) => accumulator + currentValue;
    packet = this.hexStringToHexBuffer(messageArray.reduce(reducer));
    await this.sendAndVerifyConfiguration(
      this.mibandCustomService0,
      this.configCharacteristic,
      packet,
      command
    );
  },

  performOROperationOnStringBytes(firstValue, secondValue) {
    let ORValue = this.paddHex(
      (parseInt(firstValue, 16) | parseInt(secondValue, 16)).toString(16)
    );
    console.log(ORValue);
    return ORValue;
  },

  parsePositionToString(pos) {
    let value = this.paddHex(parseInt(pos, 10).toString(16));
    return value;
  },

  /**
   * Activates display when wrist is lifted
   * @param {string} type, on, off, schedule
   */
  activateDisplayOnWristLift: async function(type, startTime, endTime) {
    let packet = "";
    let message = "";
    let command = this.messages.setup.displayOnLift;
    if (type === "on") {
      let on = "01";
      message = command + on;
    } else if (type === "off") {
      let off = "00";
      message = command + off;
    } else if (type === "schedule") {
      let startHour = this.createByteStringFromInt(startTime.getHours());
      let startMinute = this.createByteStringFromInt(startTime.getMinutes());
      let endHour = this.createByteStringFromInt(endTime.getHours());
      let endMinute = this.createByteStringFromInt(endTime.getMinutes());
      let schedule = "01";
      message =
        command + schedule + startHour + startMinute + endHour + endMinute;
    }
    packet = this.hexStringToHexBuffer(message);
    await this.sendAndVerifyConfiguration(
      this.mibandCustomService0,
      this.configCharacteristic,
      packet,
      command
    );
  },

  /**
   * Shows message when goal is reached
   * Currently only turns it off
   * @param {boolean} enable
   */
  setGoalNotification: async function(enable) {
    let packet = "";
    let message = "";
    let command = this.messages.setup.goalNotification;
    if (enable) {
      let on = "01";
      message = command + on;
    } else {
      let off = "00";
      message = command + off;
    }
    packet = this.hexStringToHexBuffer(message);
    await this.sendAndVerifyConfiguration(
      this.mibandCustomService0,
      this.configCharacteristic,
      packet,
      command
    );
  },

  /**
   * Sets support for HR supporte sleep detection
   * @param {boolean} on
   */
  setHRSleepSupport: async function(on) {
    let packet = "";
    let message = "";
    if (on) {
      message = this.messages.setup.enableHRSleepSupport;
    } else {
      message = this.messages.setup.disableHRSleepSupport;
    }
    packet = this.hexStringToHexBuffer(message);

    return this.sendWithResponse(
      this.hrMonitorService,
      this.hrMonitorControlCharacteristic,
      packet
    );
  },

  /**
   * Sets HR avaialble to non authenticated apps
   * Currently only supports disabled
   * @param {boolean} on
   */
  setExposeHRToThirdParty: async function(on) {
    let packet = ""
    let command = this.messages.setup.HRExpose
    let message = ""
    if (on) {
      message = command + "01"
    } else {
      message = command + "00"
    }

    packet = this.hexStringToHexBuffer(message);

    await this.sendAndVerifyConfiguration(
      this.mibandCustomService0,
      this.configCharacteristic,
      packet,
      command
    );
  },

  /**
   * Sets the HR measurement interval
   * @param {number} interval
   */
  setHeartRateMeasurementInterval: async function(interval) {
    // if hrMonitorControlCharacteristic can send notifications, register to them and observe response
    let intervalString = this.createByteStringFromInt(interval);

    let packet = this.hexStringToHexBuffer(
      this.messages.setup.setHRInterval + intervalString // convert interval to hex
    );
    return this.sendWithResponse(
      this.hrMonitorService,
      this.hrMonitorControlCharacteristic,
      packet
    );
  },

  /**
   * Technically does not turn off the measurement interval. Instead it sets it to a high amount of minutes.
   */
  turnOffHRMeasurementInterval: async function() {
    this.setHeartRateMeasurementInterval(256); // Once every 24 hrs
  },

  /** Assume all parameter values are strings
   * @param alias: The alias should most likely correspond to a 32 bit UUID
   * @param height: Should be divided up in two bytes (in case someone is taller than 255cm xD)
   * @param weight: Is multiplied by 200 and divided up into two bytes (why 200?)
   * @param birthYear: Divided up into two bytes
   * @param sex: 0x00 for Male and 0x01 for Female
   **/
  setUser: async function(
    height, // number
    weight, // number
    birthYear, // number
    birthMonth, // number
    birthDay, // number
    sex, // boolean
    alias // 32bit UUID
  ) {
    let heightString = this.paddHexToBytes(
      this.createByteStringFromInt(height),
      4
    );
    let height1 = heightString.substring(2, 4); // Is this the right order? Little or big endian, not certain, needs to ask Dario.
    let height2 = heightString.substring(0, 2);

    let weightString = this.paddHexToBytes(
      this.createByteStringFromInt(weight * 200),
      4
    );
    let weight1 = weightString.substring(2, 4);
    let weight2 = weightString.substring(0, 2);

    let birthYearString = this.createByteStringFromInt(birthYear);
    let birthYear1 = birthYearString.substring(2, 4);
    let birthYear2 = birthYearString.substring(0, 2);
    let birthMonth1 = this.createByteStringFromInt(birthMonth);
    let birthDay1 = this.createByteStringFromInt(birthDay);

    let sex1 = sex ? "01" : "00"; // 01 if true/female, 00 if false/male

    let alias1 = alias.substring(6, 8);
    let alias2 = alias.substring(4, 6);
    let alias3 = alias.substring(2, 4);
    let alias4 = alias.substring(0, 2);

    let packet = this.hexStringToHexBuffer(
      this.messages.setup.sendUserInfo +
        "00" +
        "00" +
        birthYear1 +
        birthYear2 +
        birthMonth1 +
        birthDay1 +
        sex1 +
        height1 +
        height2 +
        weight1 +
        weight2 +
        alias1 +
        alias2 +
        alias3 +
        alias4
    );
    await this.sendWithResponse(
      this.mibandCustomService0,
      this.userCharacteristic,
      packet
    );
  },

  createByteStringFromInt(value) {
    let hexInt = parseInt(value, 10);
    let byteString = this.paddHex(hexInt.toString(16));
    return byteString;
  },

  paddHex: function(hexValue) {
    let paddedHexValue = "";
    if (hexValue.length % 2 !== 0) {
      paddedHexValue += "0" + hexValue;
    } else {
      return hexValue;
    }
    return paddedHexValue;
  },

  paddHexToBytes: function(hexValue, bytes) {
    let paddedHexValue = hexValue.slice();
    while (paddedHexValue.length % bytes !== 0) {
      paddedHexValue = "0" + paddedHexValue;
    }
    return paddedHexValue;
  },

  // STORED DATA

  /**
   * Fetches stored data from a given date
   * @returns a promise which is solved if the fecthing starts
   * @param {date} startDate
   * @param {function} dataCallback callback function with data in it. Example data: { timestamp: date, activityType: 1, intensity: 30, steps: 10, heartRate: 65, buffer: Uint8Array }
   */
  fetchStoredData: async function(startDate, dataCallback) {
    console.log("Fetching stored data at start date:", startDate);
    let actualStartDate; // actual start date as communicated by the watch
    let sampleCounter = 0;
    let totalSamples = 0
    // register to storage control
    this.registerNotification(
      this.mibandCustomService0,
      this.storageControlCharacteristic
    );
    return new Promise((resolve, reject) => {
      ble.startNotification(
        this.deviceId,
        this.mibandCustomService0,
        this.storageControlCharacteristic,
        responseData => {
          console.log(
            // TODO: remove this console.log at some points
            "Response fetch:",
            JSON.stringify(Buffer.from(responseData))
          );
          
          let dataHex = Buffer.from(responseData).toString("hex");
          if (dataHex.substring(0, 6) === "100101") {
            console.log("Starting fetch activity");
            actualStartDate = this.createDateFromHexString(
              dataHex.substring(14, 26)
            );
            console.log(actualStartDate);
            totalSamples = this.getTotalSamplesFromBuffer(Buffer.from(responseData))
            console.log('Total samples:', totalSamples)
            sampleCounter = 0;
            // here we know we should receive data, so we register for the characteristic
            this.registerNotification(
              this.mibandCustomService0,
              this.storageDataCharacteristic
            );
            ble.startNotification(
              this.deviceId,
              this.mibandCustomService0,
              this.storageDataCharacteristic,
              dataResponse => {
                // got data!
                let buffer = new Uint8Array(dataResponse);
                dataCallback(
                  this.createSingleActivitySamplesFromSeveral(
                    actualStartDate,
                    sampleCounter,
                    buffer
                  )
                );
                sampleCounter += Math.floor(buffer.length / 4);
              },
              reject
            );

            // start fetch sequence
            this.sendFetchCommand().catch(reject);
          }
          if (dataHex === "100201") {
            // First fetch completed, checks if there are other batches left, because there should be since we didn't receive 100204
            // The next date is the last date which is the startDate + totalSamples (each sample being a minute) and
            // adding 15 minutes to make sure that we are asking for the "next memory block" and not anything we had previously.
            let fifteenMinutes = 15 * 60 * 1000
            let nextStartDate = new Date (actualStartDate.getTime() + totalSamples * 1000 * 60 + fifteenMinutes)
            console.log("Next start date:", nextStartDate, "Prev start date:", actualStartDate)
            actualStartDate = nextStartDate
            this.sendStartDateAndActivity(nextStartDate, 1)
          } else if (dataHex === '100204') {
            // Fetch finished, received when no data is available fetching directly after authentication. (auth removes all data)
            resolve()
          }
          // TODO: if a date is not accepted and watch returns NACK then reject
        },
        reject
      );

      this.sendStartDateAndActivity(startDate, 1).catch(reject);
    });
  },

  getTotalSamplesFromBuffer(buffer) {
    let totalSamples = (buffer[6] << 24) | (buffer[5] << 16) | (buffer[4] << 8) | (buffer[3])
    console.log('TS Binary:', totalSamples.toString(2))
    return totalSamples
  },

  /**
   * Creates an array of sample objects, TODO: add buffer to sample
   * @param {Buffer} samples Contains 1-4 samples of recorded activity
   */
  createSingleActivitySamplesFromSeveral(
    actualStartDate,
    amountOfSamples,
    samples
  ) {
    let packageNumber = samples[0]; // First item in buffer array is always the package number
    let sampleObjectArray = [];
    for (let i = 1; i < samples.length; i += 4) {
      let sample = {
        timestamp: new Date(
          actualStartDate.getTime() + 60000 * amountOfSamples++
        ), // adding minutes to the start date
        activityType: samples[i],
        intensity: samples[i + 1],
        steps: samples[i + 2],
        heartRate: samples[i + 3]
      };
      sampleObjectArray.push(sample);
    }
    return sampleObjectArray;
  },

  createDateFromHexString: function(hexString) {
    console.log(hexString);
    let year = parseInt(
      hexString.substring(2, 4) + hexString.substring(0, 2),
      16
    );
    let month = parseInt(hexString.substring(4, 6), 16) - 1;
    let day = parseInt(hexString.substring(6, 8), 16);
    let hour = parseInt(hexString.substring(8, 10), 16);
    let minutes = parseInt(hexString.substring(10, 12), 16);
    let seconds = 0;
    let date = new Date(year, month, day, hour, minutes, seconds);
    console.log(date);
    return date;
  },

  // activity type not currently supported, only fetches activity 01
  sendStartDateAndActivity: async function(date, activityType) {
    console.log("Sending first packet");
    let customDate = new CustomDate(date);
    let dateMessage = customDate.getDateStringPacket();
    let packet = this.hexStringToHexBuffer(
      this.messages.storedData.startDate +
        this.messages.storedData.activityType +
        // TODO: convert date to bytes string
        dateMessage.substring(0, 16) // Removes last 3 bytes because they shouldn't be sent in this case (milliseconds in 3 bytes representing fractions are not send for some reason)
    );
    return this.sendWithoutResponse(
      this.mibandCustomService0,
      this.storageControlCharacteristic,
      packet
    );
  },

  sendFetchCommand: async function() {
    let packet = this.hexStringToHexBuffer("02");
    return this.sendWithoutResponse(
      this.mibandCustomService0,
      this.storageControlCharacteristic,
      packet
    );
  },

  // Real-time step count measurement

  /**
   * Starts monitoring steps
   * @param {function} dataCallback called when step count is available
   * @param {function} errorCallback
   */
  startStepCountMonitoring: async function(dataCallback) {
    this.registerNotification(
      this.mibandCustomService0,
      this.stepCountCharacteristic
    );
    return new Promise((res, rej) => {
      ble.startNotification(
        this.deviceId,
        this.mibandCustomService0,
        this.stepCountCharacteristic,
        dataResponse => {
          let data = Buffer.from(dataResponse).toString("hex");
          console.log("Step count:", data);
          dataCallback({
            timestamp: new Date(),
            steps: this.parseStepCount(this.convertToLittleEndian(data))
          });
        },
        failure => {
          rej();
        }
      );
    });
  },

  convertToLittleEndian: function(value) {
    let littleEndian = value
      .match(/../g)
      .reverse()
      .join("");
    return littleEndian;
  },

  parseStepCount: function(value) {
    let parsedValue = "";
    for (let i = 0; i < value.length; i += 2) {
      if (!(value.charAt(i) === "0" && value.charAt(i + 1) === "0")) {
        parsedValue += value.charAt(i) + value.charAt(i + 1);
      }
    }
    return parseInt(parsedValue, 16);
  },

  // Raw data measurement

  /**
   * Starts raw data monitoring
   * @param {boolean} ppg if true streams PPG raw data
   * @param {boolean} acceleration if true streams raw accelerometry
   * @param {function} dataCallback called when data is available
   */
  startRawDataMonitoring: async function(ppg, acceleration, dataCallback) {
    await this.turnOffHRMeasuring();
    this.registerNotification(
      this.mibandCustomService0,
      this.rawDataCharacteristic
    );
    return new Promise((res, rej) => {
      ble.startNotification(
        this.deviceId,
        this.mibandCustomService0,
        this.rawDataCharacteristic,
        dataResponse => {
          const dataArray = [...new Uint8Array(dataResponse)];
          let type = "";
          console.log(dataArray);
          if (dataArray[0] === 1) {
            type = "ACC";
            if (dataArray.length >= 16) {
              dataCallback(
                {
                  timestamp: new Date(),
                  data: this.createSeveralFromSingleRawData(
                    dataArray.splice(2),
                    type
                  )
                },
                type
              );
            }
          } else if (dataArray[0] === 2) {
            type = "HR";
            dataCallback(
              {
                timestamp: new Date(),
                data: this.createSeveralFromSingleRawData(
                  dataArray.splice(2),
                  type
                )
              },
              type
            );
          }
          // TODO?
          // dataCallback({
          //   type: type,
          //   data: {
          //     x: 12,
          //     y: 0,
          //     z: 333
          //   }
          //   OR
          //   data: 2323
          // })
        },
        failure => {
          console.log("Failed to start raw HR notifications:", failure);
          rej();
        }
      );

      //Error callback, if register to notifications doesnt work (same as rej), second if there is a disconnect, if disconnect callback is called then errorcallback is not necessary
      if (ppg && acceleration) {
        let enableMessage = this.messages.rawData.rawHRAndACC;
        this.enableRawData(enableMessage)
          .then(this.turnOnContinuousHRMeasuring())
          .then(this.sendStartRawDataCommand())
          .then(
            this.pingRawData(
              this.messages.rawData.rawHRAndACC,
              this.messages.rawData.ping
            )
          )
          .then(res())
          .catch(rej());
      } else if (acceleration) {
        let enableMessage = this.messages.rawData.rawAcc;
        this.enableRawData(enableMessage)
          .then(this.sendStartRawDataCommand())
          .then(
            this.pingRawData(
              this.messages.rawData.rawAcc,
              this.messages.rawData.ping
            )
          )
          .then(res())
          .catch(rej()); // '0x02' to sensor characteristic
      } else if (ppg) {
        let enableMessage = this.messages.rawData.rawHR;
        this.enableRawData(enableMessage)
          .then(this.turnOnContinuousHRMeasuring())
          .then(this.sendStartRawDataCommand())
          .then(
            this.pingRawData(
              this.messages.rawData.rawHR,
              this.messages.rawData.ping
            )
          )
          .then(res());
      } else {
        rej(); // Parameters are incorrect.
      }
    });
  },

  createSeveralFromSingleRawData: function(rawData, type) {
    let rawDataArray = [];
    for (let i = 0; i < rawData.length; i += 2) {
      if (type === "HR") {
        let tot = (rawData[i + 1] << 8) | rawData[i];
        rawDataArray.push(tot);
      } else if (type === "ACC") {
        let tot = (rawData[i + 1] << 8) | rawData[i];
        let value = this.fromTwosComplement(tot, 2);
        rawDataArray.push(value);
      }
    }
    return rawDataArray;
  },

  // taken from http://www.java2s.com/example/nodejs/number/converts-the-given-twos-complement-representation-to-the-represented.html
  fromTwosComplement: function(twosComplement, numberBytes) {
    var numberBits = (numberBytes || 1) * 8;

    if (twosComplement < 0 || twosComplement > (1 << numberBits) - 1)
      throw "Two's complement out of range given " +
        numberBytes +
        " byte(s) to represent.";

    // If less than the maximum positive: 2^(n-1)-1, the number stays positive
    if (twosComplement <= Math.pow(2, numberBits - 1) - 1)
      return twosComplement;

    // Else convert to it's negative representation
    return -((~twosComplement & ((1 << numberBits) - 1)) + 1);
  },

  enableRawData: async function(message) {
    let packet = this.hexStringToHexBuffer(message);
    await this.sendWithoutResponse(
      this.mibandCustomService0,
      this.sensorCharacteristic,
      packet
    );
  },

  sendStartRawDataCommand: async function() {
    let packet = this.hexStringToHexBuffer("02");
    return this.sendWithoutResponse(
      this.mibandCustomService0,
      this.sensorCharacteristic,
      packet
    );
  },

  pingRawData: async function(ping1, ping2) {
    this.rawDataTimer =
      this.rawDataTimer ||
      setInterval(() => {
        let packet = this.hexStringToHexBuffer(ping1);
        this.sendWithoutResponse(
          this.mibandCustomService0,
          this.sensorCharacteristic,
          packet
        );
        packet = this.hexStringToHexBuffer(ping2);
        this.sendWithoutResponse(
          this.mibandCustomService0,
          this.sensorCharacteristic,
          packet
        );
      }, 30000);
    return;
  },

  stopRawNotifications: async function() {
    clearInterval(this.rawDataTimer);
    this.rawDataTimer = undefined;
    let packet = this.hexStringToHexBuffer("03");
    await this.sendWithoutResponse(
      // Stopping notifications first and then sending without response does not work. Why???
      this.mibandCustomService0,
      this.sensorCharacteristic,
      packet
    );

    return this.stopNotification(
      this.mibandCustomService0,
      this.rawDataCharacteristic
    );
  },

  // Real-time HR measurement

  startHRContinuousMonitoring: async function(callback) {
    this.startHRNotifications(callback);
    await this.turnOffHRMeasuring();
    await this.turnOnContinuousHRMeasuring();
    await this.pingHRControlMonitor();
    return;
  },

  stopHRContinuousMonitoring: async function() {
    clearInterval(this.hrPingTimer);
    this.hrPingTimer = undefined;
    return this.turnOffContinuousHRMeasuring();
  },

  turnOffHRMeasuring: async function() {
    await this.turnOffOneShotHRMeasuring();
    return this.turnOffContinuousHRMeasuring();
  },

  turnOffOneShotHRMeasuring: async function() {
    let packet = this.hexStringToHexBuffer(
      this.messages.heartRate.turnOffHROneShot
    );
    return this.sendWithResponse(
      this.hrMonitorService,
      this.hrMonitorControlCharacteristic,
      packet
    );
  },

  turnOffContinuousHRMeasuring: async function() {
    let packet = this.hexStringToHexBuffer(
      this.messages.heartRate.turnOffHRContinuous
    );
    return this.sendWithResponse(
      this.hrMonitorService,
      this.hrMonitorControlCharacteristic,
      packet
    );
  },

  startHRNotifications: function(callback) {
    this.registerNotification(
      this.hrMonitorService,
      this.hrMonitorMeasureCharacteristic
    );

    ble.startNotification(
      this.deviceId,
      this.hrMonitorService,
      this.hrMonitorMeasureCharacteristic,
      responseData => {
        let value = Buffer.from(responseData);
        console.log(value);
        let hrValue = "0x" + value.toString("hex");
        console.log("HR Value: " + hrValue);
        callback(parseInt(hrValue));
      },
      failure => {
        console.log(failure);
      }
    );
  },

  turnOnContinuousHRMeasuring: async function() {
    let packet = this.hexStringToHexBuffer(
      this.messages.heartRate.turnOnHRContinuous
    );
    return this.sendWithResponse(
      this.hrMonitorService,
      this.hrMonitorControlCharacteristic,
      packet
    );
  },

  pingHRControlMonitor: async function() {
    this.hrPingTimer =
      this.hrPingTimer ||
      setInterval(() => {
        let packet = this.hexStringToHexBuffer(this.messages.heartRate.ping);
        this.sendWithResponse(
          this.hrMonitorService,
          this.hrMonitorControlCharacteristic,
          packet
        );
      }, 12000);
    return;
  },

  // Stop HR Monitoring, both raw and real-time. This needs an own function because the notifications need to stop after messages have been sent for some reason.

  stopHRMonitoring: async function() {
    this.stopRawNotifications();
    this.stopHRContinuousMonitoring();
  },

  // Setting and getting device time, battery, hw info and sw info.

  getTimeStatus: async function() {
    let data = await this.read(
      this.mibandCustomService0,
      this.timeCharacteristic
    );
    let dataBuffer = Buffer.from(data);

    let year = dataBuffer.readUInt16LE(0),
      mon = dataBuffer[2] - 1,
      day = dataBuffer[3],
      hrs = dataBuffer[4],
      min = dataBuffer[5],
      sec = dataBuffer[6];
    return new Date(year, mon, day, hrs, min, sec).toLocaleString();
  },

  setTimeStatus: async function(date) {
    console.log("Setting time status with:", date);

    let customDate = new CustomDate(date);
    let packetContent = customDate.getDateStringPacket();

    let packet = this.hexStringToHexBuffer(packetContent);

    return this.sendWithResponse(
      this.mibandCustomService0,
      this.timeCharacteristic,
      packet
    );
  },

  getBatteryStatus: async function() {
    let data = await this.read(
      this.mibandCustomService0,
      this.batteryCharacteristic
    );
    let dataBuffer = Buffer.from(data);
    let chargeLevel = dataBuffer[1];
    return chargeLevel + "%";
  },

  getHardwareInfo: async function() {
    let data = await this.read(
      this.deviceInformationService,
      this.hardwareCharacteristic
    );
    let dataString = this.dataToASCII(new Uint8Array(data));
    return dataString;
  },

  getSoftwareInfo: async function() {
    let data = await this.read(
      this.deviceInformationService,
      this.softwareCharacteristic
    );
    let dataString = this.dataToASCII(new Uint8Array(data));
    return dataString;
  },

  // BLE communication functions

  read: async function(service, characteristic) {
    return new Promise((res, rej) => {
      ble.read(
        this.deviceId,
        service,
        characteristic,
        responseData => {
          console.log("Reading packet:", JSON.stringify(responseData));
          res(responseData);
        },
        failure => {
          console.log(failure);
          alert("Failed to read characteristic from device.");
          rej();
        }
      );
    });
  },

  sendWithoutResponse: async function(service, characteristic, data) {
    console.log("Service:", service, "Char:", characteristic);
    var dataInBits = new Uint8Array(data);
    return new Promise((res, rej) => {
      ble.writeWithoutResponse(
        this.deviceId,
        service,
        characteristic,
        dataInBits.buffer,
        successResponse => {
          console.log("Packet sent...");
          res(successResponse);
        },
        failure => {
          console.log("Write without response failed");
          rej(failure);
        }
      );
    });
  },

  sendWithResponse: async function(service, characteristic, data) {
    var dataInBits = new Uint8Array(data);
    return new Promise((res, rej) => {
      ble.write(
        this.deviceId,
        service,
        characteristic,
        dataInBits.buffer,
        successResponse => {
          let response = Buffer.from(successResponse).toString("hex");
          console.log(response);
          if (response === "4f4b") {
            // User step goals set properly, probably also an OK response for a bunch of other messages.
            // Sleep support set properly, same confirmation response is used for several characteristics.
            res(successResponse);
          }
        },
        failure => {
          console.log("Failed to send:", failure);
          rej();
        }
      );
    });
  },

  registerNotification: function(service, characteristic) {
    if (!this.runningNotifications.has(characteristic)) {
      console.log("Registering subscription", service);
      this.runningNotificationCharacteristics.push(characteristic);
      this.runningNotifications.set(characteristic, service);
    }
  },

  stopAllNotifications: async function() {
    console.log("Stopping notifications...");
    console.log("Subscribed currently:", this.runningNotifications);
    console.log(
      "Running characteristics currently:",
      this.runningNotificationCharacteristics
    );

    while (this.runningNotificationCharacteristics.length > 0) {
      let characteristic = this.runningNotificationCharacteristics.pop();
      let service = this.runningNotifications.get(characteristic);
      this.stopNotification(service, characteristic);
      this.runningNotifications.delete(characteristic);
    }
    console.log(
      "Running characteristics:",
      this.runningNotificationCharacteristics
    );
    console.log("Subscriptions:", this.runningNotifications);
  },

  stopNotification: async function(service, characteristic) {
    console.log("Stopping:", service, characteristic);
    return new Promise((res, rej) => {
      ble.stopNotification(
        this.deviceId,
        service,
        characteristic,
        success => {
          console.log("Stopped notifications:", success);
          res();
        },
        failure => {
          console.log("Failed to stop notifications", failure);
          rej();
        }
      );
    });
  },

  generateKey: async function() {
    let keyArray = new Uint8Array(16);
    keyArray = crypto.getRandomValues(keyArray);
    this.authenticationKey = Buffer.from(keyArray).toString("hex");
    console.log("Generated key:", this.authenticationKey);
    return this.authenticationKey;
  },

  delay: async function(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  },

  dataToASCII: function(data) {
    let dataString = this.textDecoder.decode(data);
    return dataString;
  },

  hexStringToHexBuffer: function(data) {
    console.log("Creating packet from string:", data);
    let hexBuffer = Buffer.from(data, "hex");
    return hexBuffer;
  }
};

export default Miband3;
