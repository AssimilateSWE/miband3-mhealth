// import something here

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/cli-documentation/boot-files#Anatomy-of-a-boot-file
import Vuelidate from 'vuelidate'
export default async ({ Vue }) => {
  Vue.use(Vuelidate)
}
