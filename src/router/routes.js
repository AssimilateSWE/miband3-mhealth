
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { 
        path: '', component: () => import('pages/Search.vue')
      },
      {
        path: '/heart', component: () => import('pages/Heart.vue')
      },
      {
        path: '/step', component: () => import('pages/Step.vue')
      },
      {
        path: '/sleep', component: () => import('pages/Sleep.vue')
      },
      {
        path: '/deviceInfo', component: () => import('pages/DeviceInfo.vue')
      },
      {
        path: '/profile', component: () => import('pages/Profile.vue')
      },
      {
        path: '/storedData', component: () => import('pages/StoredData.vue')
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }

]

export default routes
