# Miband3 BLE mHealth app (miband3)

An app that extracts mHealth related data from the Miband3 using BLE and JS

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev -m cordova -T android - To view the app on the phone and to be able to use the BLE functionality.
quasar dev - To view the app in the browser only without BLE functionality. 
```


### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).

### Notes to current users.

To be able to run the app on your phone you need to:

- Install the dependencies (see above).
- Run the app in development mode for the phone (see above).

- Make sure the bluetooth is turned on.
- Make sure the GPS is turned on (location services in Android equates to the GPS in need of being turned on).

If its the first time you connect to the miband, when you authenticate (the bracelet vibrates and you tap it), all previous data on the bracelet is erased. This means that if you would like to see the data the miband captures, you will have to wait a few minutes for data to accumulate (while wearing it) and **then** go to the Stored data page and download the data. Note: The data is shown in the console, UI is currently underway to represent the data in a more user friendly way. 

### Acknowledgements

I would like to thank the Gadgetbridge team and their voluenteer contributors for being able to put together most of this project. 
https://gadgetbridge.org/

At the same time i would like to thank Volodymyr Shymanskyy for replying to a few emails and being overall friendly:
https://github.com/vshymanskyy

Moreover i would like to thank José Rebelo for chatting with me and helping me solve a few issues, as well as for contributing to the display functionality. 
https://github.com/joserebelo

And finally my mentor for all the valuable assistance and coaching throughout:
https://github.com/dariosalvi78

